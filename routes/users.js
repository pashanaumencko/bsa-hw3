const express = require('express');
const router = express.Router();

const { Service } = require("../services/user.service");
const { IsJSON } = require("../middlewares/auth.middleware");

router.get('/', IsJSON, (req, res, next) => {
  const result = Service.getUsers();
  res.status(200).json(result.data);
});

router.get('/:id', IsJSON, (req, res, next) => {
  const id = Number(req.params.id);
  const result = Service.getUser(id);
  if (result.error) {
    res.status(404).send(result.error);
  } else {
    res.status(200).json(result.data);
  }
});

router.post('/', IsJSON, (req, res, next) => {
  const result = Service.addUser(req.body);

  if (result.error) {
    res.status(400).send(result.error);
  } else {
    res.status(201).send(result.message);
  }
});

router.put('/:id', IsJSON, (req, res, next) => {
  const id = Number(req.params.id);
  const result = Service.updateUser(req.body, id);

  if (result.error) {
    res.status(400).send(result.error);
  } else {
    res.status(result.code).send(result.message);
  }
});

router.delete('/:id', IsJSON, (req, res, next) => {
  const id = Number(req.params.id);
  const result = Service.deleteUser(id);

  if (result.error) {
    res.status(400).send(result.error);   
  } else {
    res.status(200).send(result.message);
  }
});

module.exports = router;
