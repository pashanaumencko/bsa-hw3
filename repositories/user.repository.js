const fs = require('fs');
const path = require('path');

class Data {
  static getData() {
    const users = require('./userlist.json');
    return users;
  }

  static setData(AddedData) {
    if (AddedData) {
      let data = Data.getData();
      data.push(AddedData);

      fs.writeFileSync(path.join(__dirname, '/userlist.json'), JSON.stringify(data,
        (k, v) => Array.isArray(v) && !(v = v.filter(e => e)).length ? void 0 : v, 2), 
        (err) => {  
          if (err) throw err;
          console.log('Data written to file');
      });
      return data;
    } else {
      return false;
    }
  }

  static updateData(updatedData) {
    if(updatedData) {
      const { _id } = updatedData;
      let data = Data.getData();
      data[_id - 1] = updatedData;
      fs.writeFileSync(path.join(__dirname, '/userlist.json'), JSON.stringify(data,
        (k, v) => Array.isArray(v) && !(v = v.filter(e => e)).length ? void 0 : v, 2), 
        (err) => {  
          if (err) throw err;
          console.log('Data written to file');
      });
      return data;
    } else {
      return false;
    }
  }

  static deleteData(id) {
    if(id) {
      let data = Data.getData();
      delete data[id - 1];
      fs.writeFile(path.join(__dirname, '/userlist.json'), JSON.stringify(data, 
        (k, v) => Array.isArray(v) && !(v = v.filter(e => e)).length ? void 0 : v, 2), 
        (err) => {  
          if (err) throw err;
          console.log('Data deleted from file');
        });
      return data;
    } else {
      return false;
    }
  }
}

module.exports = {
  Data
};